#!/bin/bash

[[ $- != *i* ]] && return

[[ -f ~/etc/bashrc ]] && .~/etc/bashrc

bind "set completion-ignore-case on" #Ignore case when TAB completion
set -o emacs
shopt -s checkwinsize  # Check window size&update LINES and COLUMNS

# A. EnvVars(depend on shell session)
##Path
additional_dirs=("$HOME/.bin" "$HOME/.local/bin" "/path/to/binaries"); for dir in "${additional_dirs[@]}"; do [ -d "$dir" ] && PATH="$dir:$PATH"; done; [ -d "$HOME/.local/bin" ] && PATH="$HOME/.local/bin:$PATH"

export LD_LIBRARY_PATH="${HOME}/.local/lib64:${HOME}/.local/lib32:${HOME}/.local/lib:/usr/local/lib64:/usr/local/lib32:/usr/local/lib:${LD_LIBRARY_PATH}"

if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/Applications" ] ;
  then PATH="$HOME/Applications:$PATH"
fi

if [ -d "/var/lib/flatpak/exports/bin/" ] ;
  then PATH="/var/lib/flatpak/exports/bin/:$PATH"
fi




PS1="[\u@\h \W]\$ "

export HISTCONTROL=erasedups:ignoredups:ignorespace:ignoreboth
export HISTIGNORE="&:c:p:x:h:r:.:..:...:cd*:lc*:cl*:ls*:ll:la:ld:l.:lab:dt:ds:dn:db:df:ps:exit:clear:history*:*--help:man *"
export HISTFILESIZE=10000
export HISTSIZE=500
export HISTTIMEFORMAT="%y/%m/%d %T "

export EDITOR="vim"
export SUDO_EDITOR="${EDITOR}"
export VISUAL="${EDITOR}"
export TERM="kitty"
export BROWSER="brave-browser-nightly"
export LANG='en_US.UTF-8'              # Set the default language

export CLICOLOR=1
export LS_COLORS='no=00:fi=00:di=00;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.gz=01;31:*.bz2=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.avi=01;35:*.fli=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.ogg=01;35:*.mp3=01;35:*.wav=01;35:*.xml=00;31:'
# Color_theme for less pager
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'$\E[01;31m'
export LESS_TERMCAP_me=$'$\E[0m'
export LESS_TERMCAP_se=$'$\E[0m'
export LESS_TERMCAP_so=$'$\E[01;44;33m'
export LESS_TERMCAP_ue=$'$\E[0m'
export LESS_TERMCAP_us=$'$\E[01;32m'
# GCC warnings & errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:loc'


# B. Aliases
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'
alias chmod='chmod -v'
alias udg='sudo apt update && sudo apt upgrade -y'
alias c="clear; ls -ahl --color=auto"
alias v="vim"
alias x="exit"
alias cf='xclip -selection clipboard <'
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'  #Git Aliases(mainly using lazygit)
# git
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias stat='git status'  # 'status' is protected name so using 'stat' instead
alias tag='git tag'
alias newtag='git tag -a'


alias grep="/usr/bin/grep --color=auto"
alias diff="diff --color=auto"
alias ip="ip -color=auto"
alias sshv='ssh -v'
alias scp='scp -r'
alias ip='ip --color=auto'
alias mem='free -h'
alias wget="wget -c"
alias userlist="cut -d: -f1 /etc/passwd"
# K. Docker
alias dc='docker-compose'
alias dbuild='docker-compose build'
alias dup='docker-compose up'
alias ddown='docker-compose down'
alias d='docker'
alias dcr='docker run'
alias dk='docker ps -a'
alias dps='docker ps'
# L. Python
alias py3='python3'
alias pip='pip3'
#ps
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"
#add new fonts
alias update-fc='sudo fc-cache -fv'

alias yta-mp3="yt-dlp --extract-audio --audio-format mp3 "
alias playmp3='ffplay *.mp3'
alias playmp4='ffplay *.mp4'


#Recent Installed Packages
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"
alias riplong="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -3000 | nl"

#Cleanup orphaned packages
#alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'
#search content with ripgrep
alias rg="rg --sort path"

alias nb="$EDITOR ~/.bashrc"

#shutdown or reboot
alias ssn="sudo shutdown now"
alias sr="sudo reboot"


# P. File Navigation
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias restart='exec "${SHELL}"' #Restart the shell

set_title() {
    echo -ne "\033]2;$(pwd)\007"
}

# # ex = EXtractor for all kinds of archives
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   tar xf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

#remove
alias rmgitcache="rm -r ~/.cache/git"






# Use neovim for vim if present.
[ -x "$(command -v nvim)" ] && alias vim="nvim" vimdiff="nvim -d"

# Use $XINITRC variable if file exists.
[ -f "$XINITRC" ] && alias startx="startx $XINITRC"

[ -f "$MBSYNCRC" ] && alias mbsync="mbsync -c $MBSYNCRC"

# sudo not required for some system commands
for command in mount umount sv pacman updatedb su shutdown poweroff reboot ; do
	alias $command="sudo $command"
done; unset command

se() {
	choice="$(find ~/.local/bin -mindepth 1 -printf '%P\n' | fzf)"
	[ -f "$HOME/.local/bin/$choice" ] && $EDITOR "$HOME/.local/bin/$choice"
	;}

# Verbosity and settings that you pretty much just always are going to want.
alias \
	cp="cp -iv" \
	mv="mv -iv" \
	rm="rm -vI" \
	bc="bc -ql" \
	rsync="rsync -vrPlu" \
	mkd="mkdir -pv" \
	yt="yt-dlp --embed-metadata -i" \
	yta="yt -x -f bestaudio/best" \
	ytt="yt --skip-download --write-thumbnail" \
	ffmpeg="ffmpeg -hide_banner"

# Colorize commands when possible.
alias \
	ls="ls -hN --color=auto --group-directories-first" \
	grep="grep --color=auto" \
	diff="diff --color=auto" \
	ccat="highlight --out-format=ansi" \
	ip="ip -color=auto"

# These common commands are just too long! Abbreviate them.
alias \
	ka="killall" \
	g="git" \
	trem="transmission-remote" \
	YT="youtube-viewer" \
	sdn="shutdown -h now" \
	e="$EDITOR" \
	v="$EDITOR" \
	p="pacman" \
	xi="sudo xbps-install" \
	xr="sudo xbps-remove -R" \
	xq="xbps-query" \
	z="zathura"

alias \
	lf="lfub" \
	magit="nvim -c MagitOnly" \
	ref="shortcuts >/dev/null; source ${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc ; source ${XDG_CONFIG_HOME:-$HOME/.config}/shell/zshnameddirrc" \
	weath="less -S ${XDG_CACHE_HOME:-$HOME/.cache}/weatherreport" \




neofetch
