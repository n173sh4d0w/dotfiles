#!/bin/bash

myrepo="$HOME/myrepo"
gitthings="$HOME/Downloads/gitthings"
scripts="$HOME/Downloads/gitthings/scripts"

#Installing Dependencies (debian only)
echo "Installing Dependencies"
sudo pacman -S qtile rofi kdenlive obs-studio sxhkd xclip clipmenu krusader kio-extras discord kitty ranger dunst mpd ncmpcpp mpv 
neofetch qutebrowser pulsemixer zsh git neovim flatpak picom && paru -S nerd-fonts-complete feh ueberzug

#Download Dots
echo "Downloading Dotfiles"
cd && git clone https://gitlab.com/thelinuxcast/my-dots.git

mv my-dots myrepo

cd $myrepo
ln -s $HOME/myrepo/dwm ~/.config
ln -s $HOME/myrepo/kitty ~/.config
ln -s $HOME/myrepo/bash ~/.config
ln -s $HOME/myrepo/dunst ~/.config
ln -s $HOME/myrepo/mpv ~/.config
#rm -r ~/.config/neofetch && ln -s $HOME/myrepo/neofetch ~/.config
ln -s $HOME/myrepo/picom ~/.config
ln -s $HOME/myrepo/lf ~/.config
ln -s $HOME/myrepo/bravebrowser ~/.config
ln -s $HOME/myrepo/demenu ~/.config

cd $gitthings && git clone https://gitlab.com/thelinuxcast/scripts.git
cd $scripts
sudo cp *.sh weather.py /usr/local/bin

cd $HOME/.config
git clone https://gitlab.com/thelinuxcast/nvim.git
