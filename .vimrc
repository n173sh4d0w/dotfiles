"" GENERAL
set nocompatible
syntax on
set number
set history=1000
set showmode
set visualbell
set noerrorbells
set autoread
set gcr=a:blinkon0
set relativenumber
set encoding=UTF-8
set nowrap
set linebreak
set autowrite
set scrolloff=8
set showmatch
set hidden
set fillchars+=vert:\
set cursorline
set cursorcolumn
set confirm
set ruler
set cmdheight=1
set path+=**					" Searches current directory recursively.
set wildmenu
set clipboard=unnamedplus       " Copy/paste between vim and other programs.
set incsearch ignorecase smartcase hlsearch
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L
set noswapfile nobackup nofoldenable
set autoindent smartindent smarttab shiftwidth=4 softtabstop=4 tabstop=4 expandtab
set guioptions-=m  " Remove menu bar
set guioptions-=T  " Remove toolbar
set guioptions-=r  " Remove right-hand scroll bar
set guioptions-=L  " Remove left-hand scroll bar
filetype plugin indent on
"" Autocmd
" Auto-compile suckless progs/c files
autocmd BufWritePost config.h,config.def.h !sudo make install
" Auto-compile on Save for C programs
autocmd BufWritePost *.c !gcc % -o %< -Wall
" Disable automatic commenting on newline
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" Automatically delete trailing whitespace and newlines on save & reset cursor position
autocmd BufWritePre * let currPos = getpos(".") | %s/\s\+$//e | %s/\n\+\%$//e | %s/\%$/\r/e
" Restore cursor position in file
autocmd BufWritePre * cal cursor(currPos[1], currPos[2])
" Auto-save Files
autocmd BufWritePre * :wa
" Auto-close Pairs (e.g., parentheses, brackets)
autocmd FileType python,rust,html,javascript inoremap <buffer> <CR> <CR><C-o>O
" Auto-fold Based on Syntax
autocmd FileType python,rust,html,javascript set foldmethod=syntax
" Auto-refresh Netrw on file operations
autocmd BufEnter * if (winnr("$") == 1 && exists("b:netrw_localrmdir")) | q | endif
"" MAPPING
" map, to buffer; cnoremap,inoremap,nnoremap: CLI/insert/normal mode mapping
" Mapping Leader to space
let mapleader = " "

" Gnl
" Switch btw open buffers
nnoremap <leader>b :ls<CR>:b<Space>
" Toggle  h-term split
nnoremap <Leader>tt :belowright split +terminal\ ++cols=30<CR>
" Savequit with leader key
nnoremap <leader>wq :wq<CR>
" Set netrw vsplit width
let g:netrw_winsize = 7
" Toggle netrw as vertical split
nnoremap <Leader>n :Vexplore<CR>
" Enable live preview in Netrw
let g:netrw_preview = 1
" Map ii to Escape in Visual mode
vnoremap ii <Esc>

"FuzzFinding_fzf
" file search
nnoremap <leader>f :Files<CR>
nnoremap <leader>g :GFiles<CR>
" within open buffers
nnoremap <leader>b :Buffers<CR>
" search tags
nnoremap <leader>t :Tags<CR>

" NavWM
" Change 2 split windows from vert to horiz or horiz to vert
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K
" Switch btw splitWin
set splitbelow splitright
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
" Resize splitWin
nnoremap <C-Left> :vertical resize -5<CR>
nnoremap <C-Right> :vertical resize +5<CR>
nnoremap <C-Up> :resize -5<CR>
nnoremap <C-Down> :resize +5<CR>

" Git
" Git status
nnoremap <leader>gs :Git<CR>
" Git add current file
nnoremap <leader>ga :Git add %<CR>
" Git commit
nnoremap <leader>gc :Git commit<CR>
" Git push
nnoremap <leader>gp :Git push<CR>

"Editing
" Toggle line wrapping
nnoremap <leader>ww :set wrap!<CR>
" Smart home key
nnoremap <leader>h ^

nnoremap <C-f> :Files!<CR>

nnoremap S :%s//g<Left><Left>
nmap <Leader>r :w<CR>:so %<CR>
map <Leader>e $
nmap <F8> :TagbarToggle<CR>

"" FOLDING
" za to toggle  http://vimcasts.org/episodes/writing-a-custom-fold-expression/
" Toggle open/close folding with double leader key
nnoremap <Leader><Leader> za
vnoremap <Leader><Leader> zf
" Defines a foldlevel for each line of code
function! VimFolds(lnum)
  let s:thisline = getline(a:lnum)
  if match(s:thisline, '^"" ') >= 0
    return '>2'
  elseif match(s:thisline, '^""" ') >= 0
    return '>3'
  elseif line(a:lnum) + 2 <= line('$')
    let s:line_1_after = getline(a:lnum+1)
    let s:line_2_after = getline(a:lnum+2)
    if match(s:thisline, '^"""""') >= 0 &&
          \ match(s:line_1_after, '^"  ') >= 0 &&
          \ match(s:line_2_after, '^""""') >= 0
      return '>1'
    endif
  endif
  return '='
endfunction

function! VimFoldText()
  let s:info = '('.string(v:foldend-v:foldstart).' l)'
  if v:foldlevel == 1
    let s:line = ' ◇ '.getline(v:foldstart+1)[3:-2]
  elseif v:foldlevel == 2
    let s:line = '   ●  '.getline(v:foldstart)[3:]
  elseif v:foldlevel == 3
    let s:line = '     ▪ '.getline(v:foldstart)[4:]
  endif
  if strwidth(s:line) > 80 - len(s:info) - 3
    return s:line[:79-len(s:info)-3+len(s:line)-strwidth(s:line)].'...'.s:info
  else
    return s:line.repeat(' ', 80 - strwidth(s:line) - len(s:info)).s:info
  endif
endfunction

augroup fold_vimrc
  autocmd!
  autocmd FileType vim setlocal foldmethod=expr |
        \ setlocal foldexpr=VimFolds(v:lnum) |
        \ setlocal foldtext=VimFoldText() |
        \ setlocal foldcolumn=2 foldminlines=2
augroup END

"" PLUGINS
" Plugins :PlugInstall, :PlugUninstall ; https://github.com/junegunn/vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
execute 'silent !curl -fLo ' . data_dir . '/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/m' | autocmd VimEnter * PlugInstall --sync | source $MYVIMRC

call plug#begin()
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'dense-analysis/ale'
  Plug 'scrooloose/syntastic'
  Plug 'sheerun/vim-polyglot'
  Plug 'rip-rip/clang_complete'
  Plug 'octol/vim-cpp-enhanced-highlight'
  Plug 'vim-python/python-syntax'
  Plug 'ap/vim-css-color'
  Plug 'tpope/vim-fugitive'

  Plug 'honza/vim-snippets'
  Plug 'sirver/ultisnips'

  Plug 'tpope/vim-vinegar'
  Plug 'godlygeek/tabular'
  Plug 'yggdroot/indentline'
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim' " Fuzzy find plugin
  Plug 'tpope/vim-commentary'
  Plug 'majutsushi/tagbar'
  Plug 'vifm/vifm.vim' 
  Plug 'tomasr/molokai'
  Plug 'vim-airline/vim-airline'

call plug#end()

" CONFIGS
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal

" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

map <Leader>vv :Vifm<CR>
map <Leader>vs :VsplitVifm<CR>
map <Leader>sp :SplitVifm<CR>
map <Leader>dv :DiffVifm<CR>
map <Leader>tv :TabVifm<CR>

" Plugin Shortcuts
nnoremap <Leader>l :Tabularize /
function! s:align()
  let p = '^\s*|\s.*\s|\s*$'
  if exists(':Tabularize') && getline('.') =~# '^\s*|' && (getline(line('.')-1) =~# p || getline(line('.')+1) =~# p)
    let column = strlen(substitute(getline('.')[0:col('.')],'[^|]','','g'))
    let position = strlen(matchstr(getline('.')[0:col('.')],'.*|\s*\zs.*'))
    Tabularize/|/l1
    normal! 0
    call search(repeat('[^|]*|',column).'\s\{-\}'.repeat('.',position),'ce',line('.'))
  endif
endfunction
" :CocInstall extensionname1 extensionname2 ...; :CocConfig;
" Lang-servers/completion/syntax
let g:ale_fix_on_save = 1
set omnifunc=ale#completion#OmniFunc
let g:ale_completion_autoimport = 1
let g:ale_disable_lsp = 1
let g:ale_sign_column_always = 1
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '--'
let g:airline#extensions#ale#enabled = 1
let g:ale_list_window_size = 5
let g:ale_floating_window_border = repeat([''], 6)
let b:ale_linters = ['markdownlint', 'vale']
let b:ale_fixers = ['prettier']
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" Statusline
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" UltiSnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"

" c-n/p to select
" Path to directory where the library can be found
let g:clang_library_path='/usr/lib/llvm-3.8/lib'

" Utilities
let g:indentLine_setColors = 0
let g:indentLine_char_list = ['|', '¦', '┆', '┊']
let g:indentLine_concealcursor = 'inc'
let g:indentLine_conceallevel = 2

let $FZF_DEFAULT_COMMAND = 'fdfind --type f --hidden --follow --exclude .git --ignore-file ~/.ignore'

colorscheme molokai
let g:molokai_original = 1

let g:auto_save = 1
let g:auto_save_events = ["InsertLeave", "TextChanged"]

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

set matchpairs+=<:>
let g:python_highlight_all = 1

" FZF for opening files
nnoremap <leader>f :FZF<CR>
" FZF for searching text in files
nnoremap <leader>g :FZF --query <CR>
" Set FZF as the default file explorer
let g:fzf_command_prefix = 'FZF'
" Use FZF for :e command
command! -bang -nargs=* -complete=file E :call fzf#vim#command('e', <q-args>, <bang>0)

