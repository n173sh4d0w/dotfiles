#!/bin/bash
# espeak_news.sh
#Dependencies: pup, espeak; 
#chmod +x espeak_news.sh

# Check if URL is provided
[ -z "$1" ] && { echo "Usage: $0 <url>"; exit 1; }
# Fetch news content from the URL, parse with pup, and use espeak to read it
curl -s "$1" | pup 'body p text{}' | espeak
